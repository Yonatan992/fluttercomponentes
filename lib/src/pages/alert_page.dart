
import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Bienvenido a la Pagina de Alerta'),
          ),
          body: Center(
            child: RaisedButton(
              child: Text('Mostrando alertas'),
              color: Colors.blueGrey[200],
              textColor: Colors.black87,
              shape: StadiumBorder(),
              onPressed: () => _mostrarAlerta(context),
            ),


          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.format_align_right),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          );
  }

    void _mostrarAlerta (BuildContext context){
        showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context){
            return AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
              title: Text('Titulo'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('Este es el contenido de la caja de la alerta de todos los dispositivos en la red'),
                  FlutterLogo(size: 50.0)
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Cancelar'),
                  onPressed: ()=>
                    Navigator.of(context).pop(),
    
                ),
                 FlatButton(
                  child: Text('Aceptar'),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                ),
              ],
              backgroundColor: Colors.greenAccent[200],

            );
          }

        );


    }

}