import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Bienvenido a la Pagina de Avatar'),
            actions: <Widget>[

              Container(
                padding: EdgeInsets.all(7.0),
                child: CircleAvatar(
                  backgroundImage: NetworkImage('https://as.com/tikitakas/imagenes/2018/08/14/portada/1534243742_367271_1534244843_noticia_normal.jpg'),
                  radius: 25.0,
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 10.0),
                child: CircleAvatar(
                  child: Text('SYP'),
                  backgroundColor: Colors.brown,

                ),
              ),
            ],
          ),
          body: Center(
            child: FadeInImage(
              image: NetworkImage('https://static01.nyt.com/images/2019/04/16/sports/16onsoccerweb-2/merlin_153612873_5bb119b9-8972-4087-b4fd-371cab8c5ba2-master1050.jpg'),
              placeholder:AssetImage('assets/ajax-loader.gif'),
              fadeInDuration: Duration(milliseconds: 200),

            ),
          ),

        );
  }
}