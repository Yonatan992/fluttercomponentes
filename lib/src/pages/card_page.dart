import 'package:flutter/material.dart';


class CardPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Tarjetas'),
        ),

        body: ListView(
          padding: EdgeInsets.all(10.0),
            children: <Widget>[
                _cardTipo1(),
                SizedBox(height: 30.0),
                _crearTipo2(),
                 SizedBox(height: 30.0),
                 _cardTipo1(),
                SizedBox(height: 30.0),
                _crearTipo2(),
                 SizedBox(height: 30.0),
                 _cardTipo1(),
                SizedBox(height: 30.0),
                _crearTipo2(),
                 SizedBox(height: 30.0),
                 _cardTipo1(),
                SizedBox(height: 30.0),
                _crearTipo2(),
                 SizedBox(height: 30.0),
                 _cardTipo1(),
                SizedBox(height: 30.0),
                _crearTipo2(),
                 SizedBox(height: 30.0),



            ],


        ),

    );
  }


         Widget _cardTipo1() {
        return Card(
          elevation: 5.0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
            child: Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.photo_album, color: Colors.green),
                      title: Text('Soy el titulo de este Articulo'),
                      subtitle: Text('Descripcion sobre el articulo del Museo X'),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        FlatButton(
                          child: Text('Cancelar'),
                          onPressed: (){},
                        ),
                           FlatButton(
                          child: Text('OK'),
                          onPressed: (){},
                        )

                      ],
                    ),
                  ],

            ),


        );

    }


        Widget _crearTipo2() {
            final card = Container(
                child: Column(
                    children: <Widget>[
                      FadeInImage(
                        image: NetworkImage('https://static.seattletimes.com/wp-content/uploads/2018/10/90a2c67c-ba17-11e8-b2d9-c270ab1caed2-1020x776.jpg'),
                        placeholder: AssetImage('assets/ajax-loader.gif'),
                        fadeInDuration: Duration(milliseconds: 200),
                        height: 300.0,
                        fit: BoxFit.cover,
                      ),
                        // Image(
                        //   image: NetworkImage('https://static.seattletimes.com/wp-content/uploads/2018/10/90a2c67c-ba17-11e8-b2d9-c270ab1caed2-1020x776.jpg'),
                        // ),
                        Container(
                              padding: EdgeInsets.all(10.0),
                              child: Text('Descripcion de la imagen'),
                        ),

                    ],


                ),

            );
            return Container(
                decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
                color: Colors.green[300],
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.green[200],
                    blurRadius: 10.0,
                    spreadRadius: 2.0,
                    offset: Offset(2.0, -10.0)

                  )

                ]
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: card,
              ),
            );

        }


}