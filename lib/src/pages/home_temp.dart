import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {

final categorias = ['Arte','Cuadros','Pinturas','Murales'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes Temp'),
      ),
      body: ListView(
        //children: _crearItems()
        children: _crearItemsCortos()
      ),


    );
  }

  List<Widget> _crearItems (){

    List<Widget> lista = new List<Widget>();
    for (String opt in categorias) {
      final tempWidget = ListTile(
        title:  Text( opt ),
      );
      lista.add(tempWidget);
      lista.add(Divider());

      
    }
    return lista;
  }

  List<Widget> _crearItemsCortos (){

    return categorias.map(( item ){
        return Column(
          children: <Widget>[
              ListTile(
                title: Text( item + '!'),
                subtitle: Text('Museo de Vias Navegables'),
                leading: Icon(Icons.add_to_photos),
                trailing: Icon(Icons.arrow_right),
                onTap: (){},
              ),
              Divider()
          ],
        );


    }).toList();


  }



}