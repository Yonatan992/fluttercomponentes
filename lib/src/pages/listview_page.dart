import 'dart:async';

import 'package:flutter/material.dart';

class ListviewPage extends StatefulWidget {

@override
  _ListviewPageState createState() => _ListviewPageState();
}

class _ListviewPageState extends State<ListviewPage> {
 
  ScrollController _scrollController = new ScrollController();
  List<int> _listaNumeros = new List();
  int _ultimoItems =0;
  bool _isLoading=false;

  @override
//metodo que no regresa nada
void initState() { 
  super.initState();
  _agregar10();
  _scrollController.addListener(() {
    if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent){
      //el if verifica que estemos al final de la pagina
      //_agregar10();

      fechData();
    }

  });
}

@override
void dispose() { 
  super.dispose();
  // para vaciar el scroll al cerrar la pagina y prevenir fugas de memoria
  _scrollController.dispose();
}


  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listas'),
      ),
      body: Stack(
          children: <Widget>[
            _crearListas(),
            _crearLoading(),

          ],
      ),
      
      
       


    );
    

  }

 Widget _crearListas() {

    return RefreshIndicator(
      //simplemente estoy pasando la referencia al metodo obtenerPagina1
      onRefresh: obtenerPagina1,
        child: ListView.builder(
         controller: _scrollController,
          //itemBuilder es el metodo que se va a encargar de redibujar los elementos que se encuentran dentro de esta lista
          // Builder cuando vemos eso, quiere decir la forma como se va a dibujar el widget
           itemCount: _listaNumeros.length,
           itemBuilder: (BuildContext context, int index){
             final imagen = _listaNumeros[index];
              return FadeInImage(

              image: NetworkImage('http://picsum.photos/500/300/?image=$imagen'),
              placeholder: AssetImage("assets/ajax-loader.gif"),
                );


            },
       ),
    );
 }


  Future<Null> obtenerPagina1()async{
    final duration = new Duration(seconds: 2);
    new Timer(duration, (){
      //borro todo su contenido
      _listaNumeros.clear();
      //para sumar las siguientes imagenes
      _ultimoItems++;
      _agregar10();

    });
    return Future.delayed(duration);

  }


  void _agregar10(){
  for (var i=1 ; i <10; i++) {
    _ultimoItems++;
    _listaNumeros.add(_ultimoItems);
  }
  setState(() {
    
  });

}

  Future fechData() async{
      _isLoading = true;
      setState(() {});
      final duration = new Duration(seconds: 2);
      new Timer(duration, respuestaHTTP);

  }

  void respuestaHTTP() {
    _isLoading=false;
    _scrollController.animateTo(
      _scrollController.position.pixels + 100,
      curve: Curves.fastOutSlowIn,
      duration: Duration(milliseconds: 250)
    );
    _agregar10();

  }

  Widget _crearLoading() {
    if (_isLoading){
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                CircularProgressIndicator(backgroundColor: Colors.redAccent),
            ],


          ),
          SizedBox(height: 15.0),
        ],
      );
     
    } else{
      return Container();
    }
  }



}