import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
@override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre ='';
  String _email='';
  String _fecha ='';
  String _opcionSeleccionada ='Estudiante';
  List<String> _especialidades = ['Estudiante','Abogado','Medico','Ingeniero'];

  //para poder guardar la fecha, permite manejar una relacion con la caja de texto de la fecha
  TextEditingController _guardarFecha = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs'),
      ),
      body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          children: <Widget>[
            _crearInputs(),
            Divider(),
            _creaEmail(),
            Divider(),
             _creaPassword(),
            Divider(),
            _crearFecha(context),
            Divider(),
            //combo box, select
             _crearDropDown(context),
            Divider(),
            _crearPersona(),

          ],

      ),


    );
  }

  Widget _crearInputs() {

    // textField es para usar un input independiante, mientras que el textFormField valida un formulario
    return TextField(

     textCapitalization: TextCapitalization.sentences,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0)
       ),
       counter: Text('Letras ${_nombre.length}'),
       hintText: 'Nombre de la Persona',
       labelText: 'nombre',
       helperText: 'Solo es el Nombre',
       suffixIcon: Icon(Icons.accessibility),
       icon: Icon(Icons.account_circle)

     ),

     onChanged: (valor){
      // setState es cuando queremos redibujar en tiemp real
       setState(() {
          _nombre = valor;
       });
       
     },

    );
  

    }

  
  Widget _creaEmail() {
       return TextField(
     keyboardType: TextInputType.emailAddress,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0)
       ),
       hintText: 'Ingrese Email',
       labelText: 'Email',
       helperText: 'example@example.com',
       suffixIcon: Icon(Icons.alternate_email),
       icon: Icon(Icons.email)

     ),

     onChanged: (valor){
      // setState es cuando queremos redibujar en tiemp real
       setState(() {
          _email = valor;
       });
       
     },

    );

  }

  Widget _creaPassword() {
      return TextField(
     obscureText: true,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0)
       ),
       hintText: 'Ingrese Password',
       labelText: 'Password',
       suffixIcon: Icon(Icons.lock_open),
       icon: Icon(Icons.lock)

     ),

     onChanged: (valor){
      // setState es cuando queremos redibujar en tiemp real
       setState(() {
          _email = valor;
       });
       
     },

    );


   }


  Widget _crearFecha(BuildContext context) {

    return TextField(
      enableInteractiveSelection: false,
      //asigna el controlador a esta caja de texto
      controller: _guardarFecha,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0)
       ),
       hintText: 'Fecha de Nacimiento',
       labelText: 'Fecha de Nacimiento',
       suffixIcon: Icon(Icons.calendar_view_day),
       icon: Icon(Icons.calendar_today)

     ),
     onTap: (){
       FocusScope.of(context).requestFocus(new FocusNode());
       _selectDate(context);
     },
    );

 }
      _selectDate(BuildContext context) async{
          DateTime picked = await showDatePicker(
              context:context,
              initialDate: new DateTime.now(),
              firstDate : new DateTime (2019),
              lastDate :  new DateTime(2020),
              locale: Locale('es','ES')
          );
          if (picked != null ){
            setState(() {
             _fecha = picked.toString(); 
             _guardarFecha.text = _fecha;
            });

          }

      }

  List<DropdownMenuItem<String>> getOpcionesDropdown() {
    List<DropdownMenuItem<String>> lista = new List();
    _especialidades.forEach((especialidad){
      lista.add(DropdownMenuItem(
          child: Text(especialidad),
          value: especialidad,
      ));

    });
    return lista;

  }


  Widget _crearDropDown(BuildContext context) {
      return Row(
        children: <Widget>[
          Icon(Icons.select_all),
          SizedBox(width: 30.0),
            DropdownButton(
        value: _opcionSeleccionada,
        items: getOpcionesDropdown(),
        onChanged: (opt){
          setState(() {
           _opcionSeleccionada = opt;
          });
        },
      )

        ],

      );
      
      
      
      

  }


   Widget _crearPersona() {
      return ListTile(
      title: Text('Nombre es: $_nombre'),
      subtitle: Text('El email es: $_email'),
      trailing: Text('Especialidad Seleccionada: $_opcionSeleccionada'),

    );

    }


 

 

}